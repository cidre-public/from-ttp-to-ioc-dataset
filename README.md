## Dataset source
Dataset is from Mordor project, and its APT29 large dataset : https://github.com/OTRF/mordor/tree/master/datasets/large/apt29

## Files
- `attacker_dir.json` is the JSON representation of the attacker's directory.
- `attacker_full.json` is the JSON representation of the attacker's procedures.
- `defender_dir.json` is the JSON representation of the defender's directory.
- `defender_eoi_full.json` is the JSON representation of the defender's logs considered to be Events of Interest.
- `parse_attacker.py` is the script to parse the attacker's procedures and the attacker's directory and to build the attacker's graph in the Neo4j database.
- `parse_defender.py` is the script to parse the defender's Events of Interest and the defender's directory and to build the defender's graph in the Neo4j database.
- `generate_plot.py` is the script to generate data for 3D curves.
- `export_atk_objects.csv` is the list of all attacker's objects.
- `exports/` is the folder containing some listings of objects and rules.
- `graph_attacker_full.svg` is the attacker's computed graph.
- `graph_defender_big_full.svg` is the defender's computed graph.

## Usage
- To build the attacker's graph: `python3 parse_attacker.py attacker_full.json attacker_dir.json`
- To build the defender's graph: `python3 parse_defender.py defender_eoi_full.json defender_dir.json`

