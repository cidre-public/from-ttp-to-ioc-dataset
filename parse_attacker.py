from pprint import pprint as pp
from py2neo import Graph
import json
import argparse

# Arguments
parser = argparse.ArgumentParser(
    description="Build graph from procedures list and attacker directory.")
parser.add_argument("attacker", type=str,
                    help="Path to the attacker exposed objects json file")
parser.add_argument("directory", type=str,
                    help="Path to the attacker directory about objects related to component in the victim network")
args = parser.parse_args()


# Graph database connection parameters
graphdb = Graph("bolt://127.0.0.1:7680", auth=("neo4j", "password"))

fake_graph = {"nodes": [], "relations": 0}


def map_related_cmp(directory):
    # link all object related to component to the components
    for cmp in directory:
        for obj in directory[cmp]:
            command = """
    MATCH (w:OBJ_A),(x:CMP_A) WHERE w.name="{0}" AND x.name="{1}" MERGE (w)-[r:related {{role: "related"}}]->(x)
            """.format(obj, cmp)
            print(command)
            graphdb.run(command)


def draw_graph(cmp, objects, eventid, parent=None):
    # Create cmp node
    command = """MERGE (m:CMP_A {{ name: "{0}" }}) """.format(cmp)

    # # if it's an involved procedure, link to parent to child
    # if parent:
    #     command += """MERGE (p:CMP {{ name: "{0}" }}) """.format(parent)
    #     command += """CREATE (m)<-[:Lateral {{eventid: "{1}"}}]-(p) """\
    #         .format(parent, eventid)

    for i, component in enumerate(objects):
        if component['value'] != "":
            # Create component node
            command += """MERGE (n{0}:OBJ_A {{ name: "{1}" }}) """ \
                .format(i, component['value'].replace('\\', '\\\\').replace('"', '\\"'))

            # Create relation between host and this component
            command += """CREATE (n{0})<-[:{2} {{eventid: "{1}", role: "{2}"}}]-(m) """\
                .format(i, eventid, component['role'])

    print(command)
    graphdb.run(command)


def parse_event(event, parent=None):
    print(event['name'])

    # Get Host
    cmp = event['cmp']

    # Get useful fields in message
    objects = []
    for field in event['obj']:
        try:
            objects.append({
                'role': field['role'].strip(),
                'value': field['value'].strip()
            })
        except KeyError as e:
            print(e, field)

    # set eventid
    eventid = event['name']
    draw_graph(cmp, objects, eventid, parent)


def main():
    with open(args.attacker) as f:
        for procedure in json.load(f):
            parse_event(procedure)
            if "invol" in procedure:
                for inv in procedure['invol']:
                    parse_event(inv, procedure['cmp'])

    with open(args.directory) as d:
        directory = json.load(d)
        map_related_cmp(directory)


if __name__ == "__main__":
    main()
