from pprint import pprint as pp
from py2neo import Graph
import ast
import json
import argparse

# Arguments
parser = argparse.ArgumentParser(
    description="Inject EoI JSON file into Neo4j database.")
parser.add_argument("traces", type=str,
                    help="Path to the traces json file")
parser.add_argument("directory", type=str,
                    help="Path to the defender directory about objects related to component in the victim network")
args = parser.parse_args()

# Graph database connection parameters
graphdb = Graph("bolt://localhost:7695", auth=("neo4j", "password"))

fake_graph = {"nodes": [], "relations": 0}

sysmon2car = {
    "Image": "image_path",
    "CommandLine": "command_line",
    "ParentImage": "parent_image_path",
    "ParentCommandLine": "parent_command_line",
    "User": "user",
    "Hashes": "hashes",
    "TargetFilename": "file_path",
    "SourceIp": "src_ip",
    "SourceHostname": "src_fqdn",
    "DestinationIp": "dest_ip",
    "DestinationHostname": "dest_fqdn",
    "ImageLoaded": "image_path",
    "SourceImage": "SourceImage",
    "TargetImage": "TargetImage",
    "StartModule": "start_module",
    "StartFunction": "start_function",
    "Device": "Device",
    "TargetObject": "key",
    "NewName": "NewName",
    "Configuration": "Configuration",
    "ConfigurationFileHash": "ConfigurationFileHash",
    "PipeName": "PipeName",
    "Query": "Query",
    "Destination": "Destination",
    "Consumer": "Consumer",
    "Filter": "Filter",
    "QueryName": "QueryName",
    "QueryResults": "QueryResults"
}

sysmon_useful_fields = {
    "1": ['Image', 'CommandLine', 'ParentImage', 'ParentCommandLine', 'User', 'Hashes'],  # Event ID 1: Process creation
    "2": ['Image', 'TargetFilename'],  # Event ID 2: A process changed a file creation time
    "3": ['Image', 'User', 'SourceIp', 'SourceHostname', 'DestinationIp', 'DestinationHostname'],
    # Event ID 3: Network connection
    "4": [],  # Event ID 4: Sysmon service state changed
    "5": ['Image'],  # Event ID 5: Process terminated
    "6": ['ImageLoaded', 'Hashes'],  # Event ID 6: Driver loaded
    "7": ['Image', 'ImageLoaded', 'Hashes'],  # Event ID 7: Image loaded
    "8": ['SourceImage', 'TargetImage', 'StartModule', 'StartFunction'],  # Event ID 8: CreateRemoteThread
    "9": ['Image', 'Device'],  # Event ID 9: RawAccessRead
    "10": ['SourceImage', 'TargetImage'],  # Event ID 10: ProcessAccess
    "11": ['Image', 'TargetFilename'],  # Event ID 11: FileCreate
    "12": ['Image', 'TargetObject'],  # Event ID 12: RegistryEvent (Object create and delete)
    "13": ['Image', 'TargetObject'],  # Event ID 13: RegistryEvent (Value Set)
    "14": ['Image', 'TargetObject', 'NewName'],  # Event ID 14: RegistryEvent (Key and Value Rename)
    "15": ['Image', 'TargetFilename', 'Hash'],  # Event ID 15: FileCreateStreamHash
    "16": ['Configuration', 'ConfigurationFileHash'],  # Event ID 16: Sysmon config state changed
    "17": ['PipeName', 'Image'],  # Event ID 17: PipeEvent (Pipe Created)
    "18": ['PipeName', 'Image'],  # Event ID 17: PipeEvent (Pipe Connected)
    "19": ['User', 'Name', 'Query'],  # Event ID 19: WmiEvent (WmiEventFilter activity detected)
    "20": ['User', 'Name', 'Destination'],  # Event ID 20: WmiEvent (WmiEventConsumer activity detected)
    "21": ['User', 'Consumer', 'Filter'],  # Event ID 21: WmiEvent (WmiEventConsumerToFilter activity detected)
    "22": ['QueryName', 'QueryResults', 'Image'],  # Event ID 22: DNSEvent (DNS query)
    "23": ['Image', 'TargetFilename', 'Hashes'],  # Event ID 23: FileDelete (A file delete was detected)
}

blacklisted_rules = [
    # med
    "Suspect Svchost Activity - AB323387",
    "Non Interactive PowerShell - 4843314F",
    # small
    "Privilege Escalation Preparation - 54225222",
    "Remote PowerShell Session - B2AA49E6"
]


def map_related_cmp(directory):
    # link all object related to component to the components
    for cmp in directory:
        for obj in directory[cmp]:
            command = """
    MATCH (w:OBJ),(x:CMP) WHERE w.name="{0}" AND x.name="{1}" MERGE (w)-[r:related {{role: "related"}}]->(x)
            """.format(obj, cmp)
            print(command)
            graphdb.run(command)


def draw_graph(cmp, objects, eventid, rule):
    # Create cmp node
    command = """MERGE (m:CMP {{ name: "{0}" }}) """.format(cmp)

    for i, component in enumerate(objects):
        if component['value'] != "" and component['value'] != "-":
            # Create component node
            command += """MERGE (n{0}:OBJ {{ name: "{1}" }}) """ \
                .format(i, component['value'].replace('\\', '\\\\').replace('"', '\\"'))

            # Create relation between host and this component
            command += """CREATE (n{0})<-[:{2} {{eventid: "{1}", role: "{2}", rule: "{3}"}}]-(m) """ \
                .format(i, eventid, component['role'], rule)

    print(command)
    graphdb.run(command)


def parse_event(trace):
    event = trace['event']
    rule = trace['rule']
    print(rule)
    print(event['SourceName'])
    # Get log source (sysmon, powershell, auditing...)
    source = event['SourceName']

    useful_fields = ""
    eventcode = ""
    to_car = ""
    if source == "Microsoft-Windows-Sysmon":
        useful_fields = sysmon_useful_fields
        eventcode = event['EventID']
        to_car = sysmon2car
    # elif source == "Microsoft-Windows-Powershell":
    #     useful_fields = powershell_useful_fields
    # elif source == "Microsoft-Windows-Security-Auditing":
    #     useful_fields = auditing_useful_fields
    else:
        return

    # Get useful fields in message
    objects = []
    for field in event:
        if field in useful_fields[str(eventcode)]:
            try:
                objects.append({
                    'role': to_car[field.strip()],
                    'value': event[field].strip()
                })
            except KeyError as e:
                print(e, field)

    # Get Host
    cmp = event['Hostname']

    # set eventid
    eventid = event['Hostname'] + "$" + str(event['RecordNumber']) + "$" + event['EventTime'].replace(' ', '_')

    if rule not in blacklisted_rules:
        draw_graph(cmp, objects, eventid, rule)


def mark_multi():
    command = """
        MATCH(a: CMP)-[]->(b:OBJ)
        WITH b, SIZE(COLLECT(a)) as comp, SIZE(COLLECT(DISTINCT a)) as comp2
        WHERE comp2 > 1
        SET b: OBJ_multi
        RETURN b, comp, comp2
        ORDER BY comp DESC
    """
    print(command)
    graphdb.run(command)


def main():
    with open(args.traces) as f:
        for trace in f.readlines():
            # parse_event(json.loads(trace))
            parse_event(ast.literal_eval(trace))

    with open(args.directory) as d:
        directory = json.load(d)
        map_related_cmp(directory)

    mark_multi()


if __name__ == "__main__":
    main()
