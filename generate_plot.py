from py2neo import Graph

# Graph database connection parameters
graphdb = Graph("bolt://localhost:7689", auth=("neo4j", "password"))

coverage = 100
exclusion_list = []
while coverage > 0:
    # Get most represented rule
    command = """
        MATCH (:CMP)-[r]->(b:OBJ)
        WITH r
        WHERE not r.rule in {0}
        RETURN r.rule as rule, count(r.rule) as count
        ORDER BY count DESC LIMIT 1
    """.format(exclusion_list)

    most_famous_rule = graphdb.run(command).data()[0]['rule']

    # Get objects connected to 4 components
    command = """
        MATCH (a:CMP)-[r]->(b:OBJ_multi)
        WHERE not r.rule in {0}
        WITH b, SIZE(COLLECT(DISTINCT a)) as cmp 
        WHERE cmp = 4
        RETURN COUNT(b.name) AS result
    """.format(exclusion_list)
    connected_4 = graphdb.run(command).data()[0]['result']

    # Get objects connected to 3 components
    command = """
            MATCH (a:CMP)-[r]->(b:OBJ_multi)
            WHERE not r.rule in {0}
            WITH b, SIZE(COLLECT(DISTINCT a)) as cmp 
            WHERE cmp = 3
            RETURN COUNT(b.name) AS result
        """.format(exclusion_list)
    connected_3 = graphdb.run(command).data()[0]['result']

    # Get objects connected to 2 components
    command = """
                MATCH (a:CMP)-[r]->(b:OBJ_multi)
                WHERE not r.rule in {0}
                WITH b, SIZE(COLLECT(DISTINCT a)) as cmp 
                WHERE cmp = 2
                RETURN COUNT(b.name) AS result
            """.format(exclusion_list)
    connected_2 = graphdb.run(command).data()[0]['result']

    # Compute objects coverage
    command = """
        MATCH (b:OBJ)-[r]-(:CMP)
        WITH b 
        WHERE not r.rule in {0}
        RETURN b.name as name ORDER BY b ASC
    """.format(exclusion_list)
    objects = graphdb.run(command).data()
    def_objects = []
    for obj in objects:
        def_objects.append(obj['name'])

    res = []
    with open('./export_atk_objects.csv') as f:
        atk_objects = f.readlines()
        for obj in atk_objects:
            if any(obj.strip().strip('\"').lower() in s.lower() for s in def_objects):
                res.append(obj.strip())
        coverage = round(len(res) / len(atk_objects) * 100, 2)

    # get count of objects
    command = """
                    MATCH (n:OBJ)-[r]-(:CMP)
                    WHERE not r.rule in {0}
                    WITH distinct n
                    RETURN count(n) AS result
                """.format(exclusion_list)
    all_objects = graphdb.run(command).data()[0]['result']

    print(len(exclusion_list), connected_4, connected_3, connected_2, all_objects, coverage)
    # print(len(exclusion_list), all_objects)
    # print(len(exclusion_list), coverage)

    # Exclude rule
    exclusion_list.append(most_famous_rule)
